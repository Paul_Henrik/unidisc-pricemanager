﻿namespace UniDisc
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Wares = new System.Windows.Forms.ListBox();
            this.labelWare = new System.Windows.Forms.Label();
            this.CBWare = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.numericAmount = new System.Windows.Forms.NumericUpDown();
            this.buttonAddToOrder = new System.Windows.Forms.Button();
            this.CBTypes = new System.Windows.Forms.ComboBox();
            this.labelType = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericAmount)).BeginInit();
            this.SuspendLayout();
            // 
            // Wares
            // 
            this.Wares.FormattingEnabled = true;
            this.Wares.Location = new System.Drawing.Point(275, 38);
            this.Wares.Name = "Wares";
            this.Wares.Size = new System.Drawing.Size(203, 407);
            this.Wares.TabIndex = 1;
            // 
            // labelWare
            // 
            this.labelWare.AutoSize = true;
            this.labelWare.Location = new System.Drawing.Point(24, 38);
            this.labelWare.Name = "labelWare";
            this.labelWare.Size = new System.Drawing.Size(33, 13);
            this.labelWare.TabIndex = 2;
            this.labelWare.Text = "Ware";
            // 
            // CBWare
            // 
            this.CBWare.AllowDrop = true;
            this.CBWare.FormattingEnabled = true;
            this.CBWare.Items.AddRange(new object[] {
            "CD",
            "DVD",
            "USB"});
            this.CBWare.Location = new System.Drawing.Point(99, 38);
            this.CBWare.Name = "CBWare";
            this.CBWare.Size = new System.Drawing.Size(121, 21);
            this.CBWare.TabIndex = 3;
            this.CBWare.SelectedIndexChanged += new System.EventHandler(this.CBType_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 123);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Amount";
            // 
            // numericAmount
            // 
            this.numericAmount.Location = new System.Drawing.Point(99, 123);
            this.numericAmount.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericAmount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericAmount.Name = "numericAmount";
            this.numericAmount.Size = new System.Drawing.Size(120, 20);
            this.numericAmount.TabIndex = 5;
            this.numericAmount.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericAmount.ValueChanged += new System.EventHandler(this.numericAmount_ValueChanged);
            // 
            // buttonAddToOrder
            // 
            this.buttonAddToOrder.Location = new System.Drawing.Point(27, 422);
            this.buttonAddToOrder.Name = "buttonAddToOrder";
            this.buttonAddToOrder.Size = new System.Drawing.Size(75, 23);
            this.buttonAddToOrder.TabIndex = 6;
            this.buttonAddToOrder.Text = "Add To Order";
            this.buttonAddToOrder.UseVisualStyleBackColor = true;
            this.buttonAddToOrder.Click += new System.EventHandler(this.buttonAddToOrder_Click);
            // 
            // CBTypes
            // 
            this.CBTypes.FormattingEnabled = true;
            this.CBTypes.Location = new System.Drawing.Point(99, 82);
            this.CBTypes.Name = "CBTypes";
            this.CBTypes.Size = new System.Drawing.Size(121, 21);
            this.CBTypes.TabIndex = 7;
            // 
            // labelType
            // 
            this.labelType.AutoSize = true;
            this.labelType.Location = new System.Drawing.Point(24, 85);
            this.labelType.Name = "labelType";
            this.labelType.Size = new System.Drawing.Size(31, 13);
            this.labelType.TabIndex = 8;
            this.labelType.Text = "Type";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(504, 474);
            this.Controls.Add(this.labelType);
            this.Controls.Add(this.CBTypes);
            this.Controls.Add(this.buttonAddToOrder);
            this.Controls.Add(this.numericAmount);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CBWare);
            this.Controls.Add(this.labelWare);
            this.Controls.Add(this.Wares);
            this.Name = "Form1";
            this.Text = "UniDisc Price Manager";
            ((System.ComponentModel.ISupportInitialize)(this.numericAmount)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ListBox Wares;
        private System.Windows.Forms.Label labelWare;
        private System.Windows.Forms.ComboBox CBWare;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numericAmount;
        private System.Windows.Forms.Button buttonAddToOrder;
        private System.Windows.Forms.ComboBox CBTypes;
        private System.Windows.Forms.Label labelType;
    }
}

