﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniDisc
{
    public class Ware
    {
        public enum Type { CD, DVD, Disc, USB };
        public string type;
        public int price;
        public List<float> prices;
        public List<int> amounts;
        public int amountOfWares;
        public float sumPrice;

        public float GetPrice()
        {
            float returnValue = prices[0];
            for (int i = 0; i < amounts.Count; i++)
            {
                if (amountOfWares >= amounts[i])
                {
                    returnValue = prices[i];
                }
            }
            return returnValue;
        }
    }
}
