﻿using System;
using System.IO;
using System.Collections.Generic;

using System.Windows.Forms;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace UniDisc
{
    public partial class Form1 : Form
    {
        public Ware temp;
        public List<Ware> shoppingList;

        public Form1()
        {
            InitializeComponent();
            Start();
        }

        private void Start()
        {
            temp = new Ware();
        }

        public void LoadJson(string fileName)
        {
            using (StreamReader r = new StreamReader(fileName))
            {
                Ware temp = new Ware();
                string json = r.ReadToEnd();
                JToken token = JToken.Parse(json);
                List<Ware> items = JsonConvert.DeserializeObject<List<Ware>>(json);
            }
        }

        //public class Ware
        //{
        //    public string type;
        //    public List<float> prices;
        //    public List<int> amounts;
        //    public int amountOfWares;
        //    public float sumPrice;

        //    public float GetPrice()
        //    {
        //        float returnValue = 0;
        //        for (int i = 0; i < amounts.Count; i++)
        //        {
        //            if (amountOfWares >= amounts[i])
        //            {
        //                returnValue = prices[i];
        //            }
        //        }
        //        return returnValue;
        //    }
        //}

        private void numericAmount_ValueChanged(object sender, EventArgs e)
        {
            temp.amountOfWares = (int)numericAmount.Value;
        }

        private void CBType_SelectedIndexChanged(object sender, EventArgs e)
        {

            switch (CBWare.Text)
            {
                case "CD":
                    string path = Path.Combine(Environment.CurrentDirectory, "..", "..", "CD_PRICES.json");
                    using (StreamReader r = new StreamReader(path))
                    {
                        temp = new Ware();
                        temp.type = "CD";
                        string json = r.ReadToEnd();
                        JToken token = JToken.Parse(json);
                        JArray types = (JArray)token.SelectToken("types");
                        for (int i = 0; i < types.Count; i++)
                        {
                            CBTypes.Items.Add(types[i].SelectToken("type"));
                        }
                        JArray amount = (JArray)token.SelectToken("types[0].amount");
                        JArray price = (JArray)token.SelectToken("types[0].price");
                        temp.prices = price.ToObject<List<float>>();
                        temp.amounts = amount.ToObject<List<int>>();
                        CBTypes.SelectedIndex = 0;
                    }
                    break;
                case "DVD":
                    break;
                case "USB":
                    break;
                default:
                    break;
            }
        }

        private void buttonAddToOrder_Click(object sender, EventArgs e)
        {
            float priceSum = 0;
            temp.amountOfWares = (int)numericAmount.Value;
            priceSum = (float)numericAmount.Value * temp.GetPrice();
            string product = (temp.type + " " + CBTypes.SelectedItem.ToString() + " " + temp.amountOfWares + " * " + temp.GetPrice() + "       " + priceSum);
            temp.sumPrice = priceSum;
            Wares.Items.Add(product);
        }
    }
}
